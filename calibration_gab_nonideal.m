% Calibration of parameterization

clear all;
load gabc_para.mat;  % load the gabbro-CO2 subsystem data
load gabh_para.mat;  % load the gabbro-H2O subsystem data
load gabbroc_data.mat;   % load the full system nonideality parameterization data

c_blkh2o  = 0.0258;   % bulk H2O content 
c_blkco2  = 0.0284;   % bulk CO2 content 

[nP, nT] = size(Rh2ogabbroc);
interval = 1;
startP   = 1;
endP     = nP;
Pgabbroc = Pgabbroc*1e-4;   % in GPa
P        = Pgabbroc(:, 1);
P_sample = P(startP:interval:endP, 1);
T        = Tgabbroc(1,:);
Rc       = Rco2gabbroc;
Rh       = Rh2ogabbroc;
Fc       = Fco2gabbroc;
Fh       = Fh2ogabbroc;
nplot    = length(P_sample);

f        = zeros(nP, nT);
Rh2o_calc= zeros(nP, nT);
Rco2_calc= zeros(nP, nT);
Fh2o_calc= zeros(nP, nT);
Fco2_calc= zeros(nP, nT);
Kh2o     = zeros(nP, nT);
Kco2     = zeros(nP, nT);

Mwh      = 18;    % molecular weight of H2O
Mwc      = 44;    % molecular weight of CO2
Wmh      = 0e4;   % in J
Wmc      = 0e4;   % in J
R_gas    = 8.314;  % gas constant

for i=1:1:nplot
    
    i
   
    Pressure = P_sample(i);  
    l        = startP + (i-1)*interval;

    Wmh      = -exp(-0.03522*Pressure^4 + 0.5204*Pressure^3 - 2.381*Pressure^2 + 3.64*Pressure - 9.995);
    Wmc      = -exp(0.009474*Pressure^4 - 0.1576*Pressure^3 + 0.9418*Pressure^2 - 2.283*Pressure + 13.37);
    
%     figure(1);
%     plot(Rc(l,:), T, 'r-', Rh(l,:), T, 'b*');
%     legend(['P = ' num2str(Pressure) 'GPa']);
%     xlabel('wt%');
%     ylabel('T(K)');
%     str   = num2str(Pressure);
%     title('phase diagram co2 and h2o in rocks str');
%     drawnow;
    
    cmaxh2o   = exp((gabh.c)*[Pressure^2; Pressure; 1]);
    Lrh2o     = exp((gabh.logLr)*[1/(Pressure)^4; 1/(Pressure)^3; 1/(Pressure)^2; 1/Pressure; 1]);
    Tmh2o     = (gabh.Tm)*[Pressure^2; Pressure; 1]; 
        
    cmaxco2   = (gabc.c);
    Lrco2     = exp((gabc.logLr)*[1/Pressure; 1]);
    Tmco2     = (gabc.Tm)*[Pressure; 1];  
    
    deltaTc   = (Tmco2 - Tmh2o)*(c_blkh2o/(c_blkco2 + c_blkh2o))^2; 
    deltaTh   = (Tmco2 - Tmh2o)*(c_blkco2/(c_blkco2 + c_blkh2o))^2;
    
    Tmco2     = Tmco2 - deltaTc;
    Tmh2o     = Tmh2o + deltaTh;
    
    Kh2o(i,:) = cmaxh2o*exp(Lrh2o*(1./T - 1/Tmh2o))/100;
    Kco2(i,:) = cmaxco2*exp(Lrco2*(1./T - 1/Tmco2))/100;
    
    fc_guess  = [1e-5; 0.5];  % close initial guess is very important for convergence. Fortunately, we know that volatile fraction is very small at Tm
    
    for j=1:nT
                
        fh2o  = @(f, c) c_blkh2o - f*c - (1-f)*c*Kh2o(i,j)*exp((Wmh*(1-c)^2)/(R_gas*T(j)));
        fco2  = @(f, c) c_blkco2 - f*(1-c) - (1-f)*(1-c)*Kco2(i,j)*exp((Wmc*c^2)/(R_gas*T(j)));

        dr1df1 = @(f, c) c*Kh2o(i,j)*exp(Wmh*(1-c)^2/(R_gas*T(j))) - c;
        dr1df2 = @(f, c) -f - (1-f)*Kh2o(i,j)*exp(Wmh*(1-c)^2/(R_gas*T(j)))*(1 - 2*c*(1-c)*Wmh/(R_gas*T(j)));
        dr2df1 = @(f, c) -(1-c) + (1-c)*Kco2(i,j)*exp(Wmc*c^2/(R_gas*T(j)));
        dr2df2 = @(f, c) f - (1-f)*Kco2(i,j)*exp(Wmc*c^2/(R_gas*T(j)))*(-1 + 2*c*(1-c)*Wmc/(R_gas*T(j)));

        fc  = fc_guess;
        r   = [fh2o(fc(1), fc(2)); fco2(fc(1), fc(2))];
        n   = 0;
        
        rnorm    = 1;
        dnorm    = 1;
        r_tol    = 1e-6;
        d_tol    = 1e-12;    % tolerance on absolute change between successive solutions
        its_tol  = 100;         
        
        while (rnorm > r_tol)&&(dnorm > d_tol)

            a11 = dr1df1(fc(1), fc(2));
            a12 = dr1df2(fc(1), fc(2));
            a21 = dr2df1(fc(1), fc(2));
            a22 = dr2df2(fc(1), fc(2));
            
            drdf  = [a11, a12; a21, a22]; 
            dfc   = - drdf\r;
            
            [a, MSGID] = lastwarn();
            warning('off', MSGID);
            
            dnorm = norm(dfc);
            
            fc    = fc + dfc;

            r     = [fh2o(fc(1), fc(2)); fco2(fc(1), fc(2))];
            rnorm = norm(r);

            n = n + 1;

            if (n==its_tol)
%                 disp(['!!! Newton solver for equilibrium f did not converge after ',num2str(its_tol),' iterations !!!']);
                
                break;
            end

        end
        
        
        if ((fc(1) <= 0)||(fc(1) >= 1)||(fc(2) <= 0)||(fc(2) >= 1)||n==its_tol)
            
            f(i,j) = 0;
            
            % shouldn't take negative fc results as the next guess, which
            % might lead to non-convergence
            
            Rh2o_calc(i,j) = c_blkh2o;
            Rco2_calc(i,j) = c_blkco2;
            Fh2o_calc(i,j) = 0;
            Fco2_calc(i,j) = 0;
            
        else
            
            f(i,j) = fc(1);
            
            fc_guess       = fc;
            Fco2_calc(i,j) = 1 - fc(2);            
            Fh2o_calc(i,j) = fc(2);
            Rco2_calc(i,j) = Fco2_calc(i,j)*Kco2(i,j)*exp((Wmc*(fc(2))^2)/(R_gas*T(j)));
            Rh2o_calc(i,j) = Fh2o_calc(i,j)*Kh2o(i,j)*exp((Wmh*(1-fc(2))^2)/(R_gas*T(j)));
        end
             
                
    end
    
      
end

%     figure(1); 
%     set(gcf, 'Color', 'w', 'InvertHardcopy', 'off', 'MenuBar', 'none');
%     set(gcf, 'ToolBar', 'none');
%     set(gcf, 'Units', 'Inches', 'Position', [0.1, 0.1, 6.5, 6]);
%     
%     h = pcolor(T-273, P, 100*f); 
%     set(h, 'EdgeColor','none');
%     ax=gca;
%     ax.Units='Inches';
%     ax.Position=[0.8, 0.8, 5, 4];
%     ax.FontName='Times New Roman';
%     ax.FontSize=14;
%     ax.XLabel.Interpreter='tex';
%     ax.YLabel.Interpreter='tex';
%     ax.XLabel.String='\it T\rm (\circC)';
%     ax.YLabel.String='\it P\rm (GPa)';
%     ax.Title.FontName='Times New Roman';
%     cb=colorbar;
%     cb.Label.FontSize=14;
%     cb.Label.FontName='Times New Roman';
% 
%     caxis([0 5.5]); cb.Limits=[0, 5.5];
%     ax.Title.String='Calc Volatile Fraction in Gab (wt%)';
%     name = 'fig1CalcVolatileFracinGab';
% 
%     print(gcf, name, '-dpdf', '-r300');
%     savefig(gcf, [name, '.fig']);
%     drawnow;

% save calibration_gab_nonideal.mat T P f Rh2o_calc Rco2_calc Fh2o_calc Fco2_calc;


